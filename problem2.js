const fs = require('fs');
const folderPath = '/home/sharun/Documents/MountBlue Projects/fsCallbacks/data/';

function textManipulator() {

    let nameFile = "FileNames.txt";

    function readFile() {
        let lipsumPath = folderPath + 'lipsum.txt';
        fs.readFile(lipsumPath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                console.log("lipsum.txt file has been read");
                convertToUpperCase(data);
            }
        });
    }

    function convertToUpperCase(data) {
        let upperCaseFile = data.toUpperCase();
        let upperCaseFileName = "upperCaseFile.txt";
        let upperCaseFilePath = folderPath + upperCaseFileName;
        fs.writeFile(upperCaseFilePath, upperCaseFile, (err) => {
            if (err) {
                console.log(err);
            } else {
                console.log("Upper case file has been created");
            }
        });

        writeFileNameToStorage(upperCaseFileName);
        convertToLowerCase(upperCaseFilePath);

    }

    function convertToLowerCase(filePath) {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let lowerCaseFile = data.toLowerCase()
                    .split(' ');
                let joinedLowerCaseText = lowerCaseFile.join('\n');
                let lowerCaseFileName = "splitToLowerCaseFile.txt";
                let lowerCaseFilePath = folderPath + lowerCaseFileName;
                fs.writeFile(lowerCaseFilePath, joinedLowerCaseText, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("split lower case file has been created");
                    }
                });
                writeFileNameToStorage(lowerCaseFileName);
                sortingFunction(lowerCaseFilePath);
            }
        });

    }

    function sortingFunction(filePath) {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let sortedFile = data.split('\n')
                    .sort()
                    .join(' ');
                let sortedFileName = "sortedFile.txt";
                let sortedFilePath = folderPath + sortedFileName;
                fs.writeFile(sortedFilePath, sortedFile, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("Text has been sorted");
                    }
                });
                writeFileNameToStorage(sortedFileName);
            }
        });

    }

    function writeFileNameToStorage(fileName) {
        let filePath = folderPath + nameFile;
        fs.appendFile(filePath, (fileName + " "), function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`Updated file names with ${fileName}`);
            }
        });
    }

    setTimeout(() => {
        let nameFilePath = folderPath + nameFile;
        fs.readFile(nameFilePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                data = data.trim();
                let filesToBeDeleted = data.split(' ');
                let filesDeleted = filesToBeDeleted.map((file) => {
                    let deleteFilePath = folderPath + file;
                    fs.unlink(deleteFilePath, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${file} deleted successfully`);
                        }
                    })
                });
            }
        });
    }, 3000);

    readFile();

}

module.exports = textManipulator;