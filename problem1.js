const fs = require('fs');
const path = require('path');

function createAndDeleteFiles(directory, countOfFiles) {

    function createDirectory(directory) {
        fs.mkdir(directory, (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log("Directory created.");
            }
        });
    }

    createDirectory(directory);

    function createFiles(directory, countOfFiles) {
        let count = 1;
        const intervalID = setInterval(() => {
            if (count < countOfFiles) {
                let file = "fileNumber" + count + ".json";
                let filePath = '/test/' + directory + '/' + file;
                let fullFilePath = path.join(__dirname, filePath);
                fs.writeFile(fullFilePath, `${file} has been created`, (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(`${file} has been created successfully`);
                    }
                });
            } else {
                clearInterval(intervalID);
            }
            count++;
        }, 1000);

    }

    createFiles(directory, countOfFiles);

    function deleteFiles(directory) {
        setTimeout(() => {
            let location = '/test/' + directory + '/';
            let filePath = path.join(__dirname, location);

            fs.readdir(filePath, (err, data) => {
                if (err) {
                    console.log(err);
                }
                let deletedFiles = data.map((file) => {
                    let fileName = '/test/' + directory + '/' + file;
                    let fileToBeDeleted = path.join(__dirname, fileName);
                    fs.unlink(fileToBeDeleted, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${file} has been deleted successfully`);
                        }
                    });
                    return true;
                });
            });
        }, 6000);
    }
    
    deleteFiles(directory);

}

module.exports = createAndDeleteFiles;
